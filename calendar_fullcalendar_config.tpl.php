{
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      defaultView: 'agendaWeek',
      firstDay: 1,
      weekends: false, // don't show saturday and sunday
      allDayText: 'ganzt&auml;gig',
      //	allDaySlot:false,
      axisFormat: 'HH:mm',
      //	date:11,
      //	month:1,
      //	year:2011,
      firstHour: 7,
      minTime:7,
      timeFormat: {
        // for agendaWeek and agendaDay
        agenda: 'HH:mm{ - HH:mm}', // 15:00 - 16:30
        // for all other views
			'': 'HH:mm'            // 07:30
      },
      columnFormat: {
        month: 'ddd',    // Mo
        week: 'ddd d.M.', // Mo 7.9.
        day: 'dddd d.M.'  // Monntag 7.9.
      },
      titleFormat: {
        month: 'MMMM yyyy',                             // September 2009
        week: "d. [ MMM yyyy]{ '&#8212;' d. MMM yyyy}", // 7. - 13. Sep. 2009
        day: 'dddd, d. MMM yyyy'                  // Dienstag, 8. Sep. 2009
      },
      buttonText: {
        today:    'Heute',
        month:    'Monat',
        week:     'Woche',
        day:      'Tag'
      },
      viewDisplay : function(view){
      },
      	loading: function(isLoading, view){
		if(isLoading){
			$(".fc-content",this).addClass("loading");
		}else{
			$(".fc-content",this).removeClass("loading");
		}
	},
      
      monthNames: ['Januar', 'Februar', 'M&auml;rz', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
      monthNamesShort: ['Jan.', 'Feb.', 'M&auml;r.', 'Apr.', 'Mai', 'Jun.', 'Jul.', 'Aug.', 'Sep.', 'Okt.', 'Nov.', 'Dez.'],
      dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
      dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],

      events: function(start, end, callback) {
        // TODO load first data onload then callbacks
        // TODO create timeout to prevent unnecassary request for quick navigation
        $.ajax({
          url: '<?= url('views/ajax') ?>',
          dataType: 'json',
          data: {
            // our hypothetical feed requires UNIX timestamps
            view_name: '<?= $view->name ?>',
            view_display_id: '<?= $view->current_display ?>',
            start: Math.round(start.getTime() / 1000),
            end: Math.round(end.getTime() / 1000)
          },
          success: function(events) {
            callback(events);
             
          },
          error : function(){
            // TODO better error handling
            alert("error loading events");
          }
        });
      },

      eventRender: function(event, element) {
        if(event.title.indexOf("<a") >= 0){
          var title = $(event.title);

          var i = $(".fc-event-inner",element);
          if(i.find(".fc-event-content").length != 0){
            var c = $(".fc-event-content",element);
            $(".fc-event-title",c).remove();
            c.append($('<div class="fc-event-title" />').append(title));
          }else{
            $(".fc-event-title",i).remove();
            i.append($('<span class="fc-event-title" />').append(title));
          }
        }
      }

}