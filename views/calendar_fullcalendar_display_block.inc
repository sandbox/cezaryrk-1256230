<?php
class calendar_fullcalendar_display_block extends calendar_plugin_display_block {

  private $granularity;
  private $time_arg;
  private $date_arg_id = -1;

  function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display,$options);
    $this->adapt_date_granularity();
  }

  function adapt_date_granularity(){
    $this->granularity = "week";
    //$this->time_arg = date('Y-\WW'); //this don't work somehow... we always get the week before...
    $this->time_arg = date('Y').'-W'.(date('W')+1); //that's why here +1
    if(isset($_GET['start']) && isset($_GET['end'])){
      $start = intval($_GET['start']);
      $end = intval($_GET['end']);
      $diff = $end - $start;
      $oneweek = 7*24*60*60; // One week in seconds
      $oneday = 24*60*60; // One week in seconds
      if( $diff > $oneweek ) {
        $this->granularity = "month";
      } else if ( $diff <= $oneday ) {
        $this->granularity = "day";
      }
      $mid = ($start+$end)/2;
      if ( $this->granularity == "month" ) {
        $this->time_arg = date('Y-m', $mid);
      } else if( $this->granularity == "day" ) {
        $this->time_arg = date('Y-m-d', $mid);
      } else {
        //$this->time_arg = date('Y-\WW', $mid);
        $this->time_arg = date('Y',$mid).'-W'.(date('W',$mid)+1);
      }
    }
  }


  function get_option($option) {
    $temp = parent::get_option($option);
    if( $option == "arguments" && isset($temp['date_argument']) ) {
      //TODO better granularity placement
      $temp['date_argument']['granularity'] = $this->granularity;



    }
    return $temp;
  }

  /**
   * Override some of the parent options.
   */
  function options(&$display) {
    parent::options($display);
    $display['style_plugin'] = 'calendar_fullcalendar_style';
  }


  function pre_execute() {
    // TODO modify fullcalendar parameter (ajax)
    $args = $this->get_option('arguments');

    $date_arg_id = 0;

    // TODO find an other method to find date argument
    foreach($args as $k=>$v){
      if($k == 'date_argument') break;
      $date_arg_id++;
    }
    $this->view->args[$date_arg_id] = $this->time_arg;
    parent::pre_execute();
  }


  function execute(){
    return parent::execute();
  }


  function get_style_type() {
    return 'calendar_fullcalendar_style';
  }


  function render() {
    // TODO add default on demand output mode
    $this->view->ajax_callback = false;    

    if(isset($_GET['start']) && isset($_GET['end'])){
      $this->view->ajax_callback = true;
    }else{

      $view_id = $this->view->_view_id = "fc-".$this->view->name."-".$this->view->current_display;
      $config = theme("calendar_fullcalendar_config",array("view"=>$this->view));
      drupal_add_js("
             (function($){ 
             		$(document).ready(function() {       
               		$('#$view_id').fullCalendar($config);          
               	});
              })(jQuery)
             ","inline");      
    }


    if(!$this->view->ajax_callback){
      $rendered = parent::render();
    }else{
      $rendered = !empty($this->view->result) || $this->view->style_plugin->even_empty() ? $this->view->style_plugin->render($this->view->result) : "";

      drupal_add_http_header('Content-Type', 'application/json; charset=utf-8');
      print $rendered;
      ajax_footer();

      exit;
    }
    return $rendered;
  }

}