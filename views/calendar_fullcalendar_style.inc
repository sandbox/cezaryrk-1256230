<?php
class calendar_fullcalendar_style extends calendar_plugin_style {

  
  /**
  * Set options
  */
  function option_definition() {
    $options = parent::option_definition();
    $options["event_title_field"] = array('default' => '');
    $options["event_date_field"] = array('default' => '');    
    return $options;
  }
  
  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {   
    parent::options_form($form, $form_state);

    $fields = array('' => t('<None>'));
    $this->get_field_labels_from_handlers('field', $fields);
  
    $form["event_title_field"] = array(
        '#type' => 'select',
        '#title' => t('Event title field'),
        '#options' => $fields,
        '#default_value' => $this->options["event_title_field"],
        '#description' => t('Select the field for titles in the fullcalendar'),
        '#required' => TRUE,
    );
    
    // TODO show only date fields!
    $form["event_date_field"] = array(
        '#type' => 'select',
        '#title' => t('Event date field'),
        '#options' => $fields,
        '#default_value' => $this->options["event_date_field"],
    	'#description' => t('Select the date field for start and end'),
    	'#required' => TRUE,
    );
        
  }
  
  
  function get_field_labels_from_handlers($handler_type, &$fields){
    foreach ($this->display->handler->get_handlers($handler_type)  as $field => $handler) {
      if (method_exists($handler, "label") && $label = $handler->label()) {
        $fields[$field] = $label;
      } else {
        $fields[$field] = $handler->ui_name();
      }
    }
  }
  
  function &get_handler($type, $lookup_element){
    $handler =& $this->display->handler->get_handlers($type);
    if(isset($handler[$lookup_element])){
      return $handler[$lookup_element];
    }
    $tmp = null;
    return $tmp;
  }
  
  
  
  function render(){
    
    $field_handler = array();
    $fields = array();
    $this->get_field_labels_from_handlers('field', $fields);
    foreach($fields as $field_name => $label){
      $field_handler[$field_name] =& $this->get_handler("field", $field_name);
    }
    
    $title_field = $this->options["event_title_field"];
    $title_handler =& $field_handler[$title_field];
    
    $date_field = $this->options["event_date_field"];
    $date_handler =& $field_handler[$date_field];
        
    // TODO validate    
    $title_alias = $title_handler->table_alias."_".$title_handler->real_field;
    $date_start_alias = $date_handler->table_alias."_".$date_handler->real_field;
    $date_end_alias = $date_handler->table_alias."_".$date_handler->real_field."2";
    
    
       
    $output = "";
    $events = array();
    foreach ($this->view->result as $idx => $item) {
      $event = new stdClass();
      
      // TODO theme title
      $event->title = $title_handler->advanced_render($item);//->{$title_alias};
      $event->start = $item->{$date_start_alias};
      $event->end   = $item->{$date_end_alias};
      
      // TODO look for configuration solution
      $event->allDay = false;
      $event->editable = false;
      
      // TODO theme
      foreach($field_handler as $field_name => &$handler){
        if(in_array($field_name,array($date_field,$title_field))) continue;
        $tmp  = $handler->advanced_render($item);
        if($tmp){
          $event->{"__".$field_name} = $tmp;
        }
      }
      
      $events[] = $event;
    }
    
    if($this->view->ajax_callback){
      return drupal_json_encode($events);
    }else{
      return parent::render();    
    }
        
    return $output;
  }
  
}