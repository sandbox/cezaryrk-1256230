<?php

/**
 * Implementation of hook_views_plugins()
 */
function calendar_fullcalendar_views_plugins() {
  $scripts = _calendar_fullcalendar_load_libraries();
  if(!$scripts){
    drupal_set_message("calendar_fullcalendar no libaries found","error");
    return array();
  }
  
  $path = drupal_get_path('module', 'calendar_fullcalendar');
  $vpath = $path . "/views";
  
  $plugin = array(
    // Main calendar display plugin.
    'display' => array(
      'calendar_fullcalendar_page' => array(
        'title' => t('Fullcalendar page'),
        'help' => t('Fullcalendar page.'),
        'handler' => 'calendar_fullcalendar_display_page',
        'path' => "$vpath",
        'parent' => 'calendar',
        'theme' => 'views_view',
        'no ui' => FALSE, // 
        //'no remove' => TRUE,
        'uses hook menu' => TRUE,
        'uses hook block' => FALSE,
        'use ajax' => TRUE,
        'use pager' => FALSE,
        'accept attachments' => FALSE,
        'admin' => t('Full Calendar page'),
	    //'base' => array("calendar"),
        'js' => $scripts['js'],
  		'css' => $scripts['css'],
      ),
  	'calendar_fullcalendar_block' => array(
          'title' => t('Fullcalendar block'),
          'help' => t('Fullcalendar block'),
          'handler' => 'calendar_fullcalendar_display_block',
          'path' => "$vpath",
          'parent' => 'calendar',
          'theme' => 'views_view',
          'no ui' => FALSE, // 
          'uses hook block' => TRUE,
          'use ajax' => TRUE,
          'use pager' => FALSE,
          'accept attachments' => FALSE,
          'admin' => t('Full Calendar block'),
          //'base' => array("calendar"),
          'js' => $scripts['js'],
    		'css' => $scripts['css'],
  ),
    ),
    // Style plugin for the navigation.
    'style' => array(
      'calendar_fullcalendar_style' => array(
        'title' => t('Fullcalendar style'),
        'help' => t('Fullcalendar style.'),
        'handler' => 'calendar_fullcalendar_style',
        'path' => "$vpath",
        'parent' => 'calendar_nav',
        'theme' => 'calendar_fullcalendar',
 		'theme file' => 'theme.inc',
    	'theme path' => "$vpath",
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
  		'uses ajax' => TRUE,
        'uses options' => TRUE,
        'type' => 'calendar_fullcalendar', // Only used on fullcalendar page or fullblock displays.
        'even empty' => TRUE,
      ),
    ),
  );
  
  return $plugin;
}